from random import randint


name = input("Hi what is your name? ")


for guess_number in range(1,6):
    month_number = randint(1, 12)
    year_number = randint(1997, 2022)

    print("Guess:", name, "were you born in", 
    month_number, "/", year_number, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("i knew it!")
        exit()
    elif guess_number == 5:
        print("i have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
